package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.View;
import model.Model;

//Controller is a Listener

public class Controller implements ActionListener {

	Model model;
	View view;

	public Controller() {}

	public void actionPerformed(ActionEvent e){
		model.incrementValue();
	} 

	public void addModel(Model m){
		this.model = m;
	} 

	public void addView(View v){
		this.view = v;
	} 

	public void initModel(int x){
		model.setValue(x);
	} 

} 
