package model;

import java.util.Observable;

//Model holds an int counter (that's all it is).
//Model is an Observable
//Model doesn't know about View or Controller

public class Model extends Observable {	
	
	private int counter;	//primitive, automatically initialised to 0

	public Model(){} 
	
	public void setValue(int value) {

		this.counter = value;
		setChanged();
		notifyObservers(counter);
		
	} 
	
	public void incrementValue() {

		++counter;
		setChanged();
		notifyObservers(counter);

	} 
	
} 
